const { connect } = require('http2');
const socketIO = require('socket.io');
const secret = "4BTbpL4xSTbqb4QLWdYQE1xbN5b4Nb9EKbTPp9FPUFNXRL93d4t8uoStg2Xq9DIg";
const express = require('express');
const app = express();
const http = require('http');
const nJwt = require('njwt');
const validate = require('uuid-validate');
var ip = process.env.IP || 'localhost';
var port = process.argv[2] || process.env.PORT || 3000;
var server = http.createServer().listen(port, ip, function () {
  console.log('Socket.IO server started at http://'+ip+':'+port);
});
const io = socketIO.listen(server);
io.set('match origin protocol', false);
io.set('origins', 'localhost:*');
io.set('origins', '*:*');

var users = {};

io.on('connect', socket => {
    let uuid = extractUuid(socket)
    if (!users[uuid]) {
        users[uuid] = [];
    }
    // console.log("user connect "+uuid)
    socket.join("room-" + uuid);
    // console.log(uuid);
    users[uuid].push(socket.id);
    socket.on('sendNotify', function (res) {
        if(!res.user_id){
            console.log("Không có id người nhận socket")
        }else{
            try {
                socket.to("room-" + res.user_id).emit("sendNotify", res);
            } catch (error) {
                console.log(error)
            }            
        }
    });
    socket.on('disconnect', (reason) => {
         // Khi client thoát thì emit cho người cùng phòng biết
        let uuid = extractUuid(socket)
        if (!users[uuid]) return;
        let ind = users[uuid].indexOf(socket.id);
        if (ind >= 0) {
        users[uuid].splice(ind, 1);
        }
        if (users[uuid].length == 0) {
        delete users[uuid];
        }
    });
    // socket.on('checkStatusUser', (response) => {
    //     socket.emit('data', response);
    // })
});

function extractUuid(socket){
    try {
        console.log(socket.handshake.query)
        if(socket.handshake.query.token)
        {
            let verifiedJwt = nJwt.verify(socket.handshake.query.token, secret);
            let body = verifiedJwt.body;
            return body.id;
        }
    } catch (error) {
        console.log(error)
    }
  }